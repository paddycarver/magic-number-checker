package checker

import (
	"github.com/pkg/errors"

	filetype "gopkg.in/h2non/filetype.v1"
)

const minBytesNeeded = 261

var ErrUnsupportedFile = errors.New("unsupported file")

type MagicNumberChecker struct {
	buf            []byte
	SupportedMIMEs []string
	MatchedMIME    string
	passed         bool
}

func (m *MagicNumberChecker) Write(b []byte) (int, error) {
	if m.MatchedMIME != "" {
		return len(b), nil
	}
	m.buf = append(m.buf, b...)
	if len(m.buf) < minBytesNeeded {
		return len(b), nil
	}
	for _, mime := range m.SupportedMIMEs {
		if filetype.IsMIME(m.buf, mime) {
			m.MatchedMIME = mime
			m.buf = nil
			return len(b), nil
		}
	}
	return 0, ErrUnsupportedFile
}

func (m *MagicNumberChecker) Close() error {
	if m.MatchedMIME != "" {
		return nil
	}
	return ErrUnsupportedFile
}
